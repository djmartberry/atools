#!/usr/bin/env bats

cmd=./apkbuild-lint
apkbuild=$BATS_TMPDIR/APKBUILD

# Remove APKBUILD_STYLE variable from the environment as that can affect results
unset APKBUILD_STYLE

assert_match() {
	output=$1
	expected=$2

	echo "$output" | grep -qE "$expected"
}

is_travis() {
	test -n "$TRAVIS"
}

retab() {
	case $(realpath "$(which unexpand)") in
		*busybox*) flag="-f";;
		*) flag="--first-only";;
	esac

	unexpand $flag -t4
}

@test 'mesa good version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=mesa
		pkgver=21.0.1
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'mesa bad version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=mesa
		pkgver=21.0.0
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '21.0.0' matches a bad version regex"
}

@test 'mesa good version tricky cornercase' {
	cat <<-__EOF__ > $apkbuild
		pkgname=mesa
		pkgver=21.0.10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'nginx good version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=nginx
		pkgver=1.2.0
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'nginx bad version' {
	cat <<-__EOF__ > $apkbuild
		pkgname=nginx
		pkgver=1.3.0
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '1.3.0' matches a bad version regex"
}

@test 'nginx bad version 2 digit' {
	cat <<-__EOF__ > $apkbuild
		pkgname=nginx
		pkgver=1.21.0
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "\[AL61\].*:Version '1.21.0' matches a bad version regex"
}

# vim: noexpandtab
