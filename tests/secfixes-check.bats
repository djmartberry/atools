#!/usr/bin/env bats

cmd=./secfixes-check
apkbuild=$BATS_TMPDIR/APKBUILD

assert_match() {
	output=$1
	expected=$2

	echo "$output" | grep -qE "$expected"
}

@test 'valid simple secfixes check' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'valid simple secfixes with multiple identifiers' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-130
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test 'missing colon in secfixes' {
	cat <<-__EOF__ > $apkbuild
		# secfixes
		#   1.0.0-r0:
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL37\].*:missing colon on 'secfixes'"
}

@test 'under-indented pkgver-pkgrel' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#  1.0.0-r0:
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL48\].*:pkgver-pkgrel indentation is 3 whitespaces"
}

@test 'over-indented pkgver-pkgrel' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#    1.0.0-r0:
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL48\].*:pkgver-pkgrel indentation is 3 whitespaces"
}

@test 'missing colon in pkgver-pkgrel' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0
		#     - CVE-2020-1000
		#   0.9.0-r1
		#     - CVE-2019-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL38\].*:missing colon on '1.0.0-r0'"
	assert_match "${lines[1]}" "\[AL38\].*:missing colon on '0.9.0-r1'"
}

@test 'under-indented security identifier' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#    - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL47\].*:Security identifier identation is 5 whitespaces"
}

@test 'over-indented security identifier' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#      - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL47\].*:Security identifier identation is 5 whitespaces"
}

@test 'missing hyphen on security identifier' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL41\].*:missing hyphen on 'CVE-2020-1000'"
}

@test 'missing hyphen on security identifier (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     CVE-2020-1000
		#     - CVE-2020-10001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL41\].*:missing hyphen on 'CVE-2020-1000'"
}

@test 'missing hyphen on security identifier (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-318
		#     CVE-2020-1000
		#     - CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL41\].*:missing hyphen on 'CVE-2020-1000'"
}

@test 'missing hyphen on security identifier (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1001
		#     CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL41\].*:missing hyphen on 'CVE-2020-1000'"
}

@test 'pkgrel is invalid (or missing)' {
	# Taken from testing/rabbitmq-server in 2020-04-27
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   3.7.17:
		#     - CVE-2015-9251
		#     - CVE-2017-16012
		#     - CVE-2019-11358
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL40\].*:invalid pkgrel"
}

@test 'pkgver is invalid' {
	# Taken from testing/rabbitmq-server in 2020-04-27
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   3.7<>3-r1:
		#     - CVE-2015-9251
		#     - CVE-2017-16012
		#     - CVE-2019-11358
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL39\].*:invalid pkgver"
}

@test 'GNUTLS: ID is empty' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is empty"
}

@test 'GNUTLS: ID is empty (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is empty"
}

@test 'GNUTLS: ID is empty (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-213 GNUTLS-SA CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is empty"
}

@test 'GNUTLS: ID is empty (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is empty"
}

@test 'GNUTLS: not composed purely by digits and hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-bad-01
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is not composed of only digits and hyphens"
}

@test 'GNUTLS: not composed purely by digits and hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-bad-01 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is not composed of only digits and hyphens"
}

@test 'GNUTLS: not composed purely by digits and hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-bad-01 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is not composed of only digits and hyphens"
}

@test 'GNUTLS: not composed purely by digits and hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-10-20 GNUTLS-SA-2020-bad-01
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID is not composed of only digits and hyphens"
}

@test 'GNUTLS: ID has invalid year' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-13-12-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad year, it needs to be in YYYY format"
}

@test 'GNUTLS: ID has invalid year (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-13-12-10 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad year, it needs to be in YYYY format"
}

@test 'GNUTLS: ID has invalid year (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-13-12-10 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad year, it needs to be in YYYY format"
}

@test 'GNUTLS: ID has invalid year (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-13-12-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad year, it needs to be in YYYY format"
}

@test 'GNUTLS: ID has a bad month' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-1-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad month, it needs to be in MM format"
}

@test 'GNUTLS: ID has a bad month (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-1-10 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad month, it needs to be in MM format"
}

@test 'GNUTLS: ID has a bad month (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-1-10 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad month, it needs to be in MM format"
}

@test 'GNUTLS: ID has a bad month (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-1-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad month, it needs to be in MM format"
}

@test 'GNUTLS: ID has months that do not exist' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-13-10
		#     - GNUTLS-SA-2020-00-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
}

@test 'GNUTLS: ID has months that do not exist (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-13-10 CVE-2020-1000
		#     - GNUTLS-SA-2020-00-10 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
}

@test 'GNUTLS: ID has months that do not exist (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-13-10 GNUTLS-SA-2020-10-20
		#     - GNUTLS-SA-2020-10-21 GNUTLS-SA-2020-00-10 CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
}

@test 'GNUTLS: ID has months that do not exist (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-13-10
		#     - GNUTLS-SA-2020-10-20 GNUTLS-SA-2020-00-10
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has invalid month, it must be between 01 and 12"
}

@test 'GNUTLS: ID has day not in DD format' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-1
		#     - GNUTLS-SA-2020-12-2
		#     - GNUTLS-SA-2020-12-3
		#     - GNUTLS-SA-2020-12-4
		#     - GNUTLS-SA-2020-12-5
		#     - GNUTLS-SA-2020-12-6
		#     - GNUTLS-SA-2020-12-7
		#     - GNUTLS-SA-2020-12-8
		#     - GNUTLS-SA-2020-12-9
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 9 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[3]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[4]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[5]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[6]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[7]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[8]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
}

@test 'GNUTLS: ID has day not in DD format (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-1 CVE-2020-1000
		#     - GNUTLS-SA-2020-12-2 CVE-2020-1001
		#     - GNUTLS-SA-2020-12-3 CVE-2020-1002
		#     - GNUTLS-SA-2020-12-4 CVE-2020-1003
		#     - GNUTLS-SA-2020-12-5 CVE-2020-1004
		#     - GNUTLS-SA-2020-12-6 GNUTLS-SA-2020-10-21
		#     - GNUTLS-SA-2020-12-7 GNUTLS-SA-2020-10-22
		#     - GNUTLS-SA-2020-12-8 GNUTLS-SA-2020-10-23
		#     - GNUTLS-SA-2020-12-9 GNUTLS-SA-2020-10-24
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 9 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[3]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[4]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[5]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[6]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[7]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[8]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
}

@test 'GNUTLS: ID has day not in DD format (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1004 GNUTLS-SA-2020-12-1 GNUTLS-SA-2020-10-20
		#     - CVE-2020-1005 GNUTLS-SA-2020-12-2 GNUTLS-SA-2020-10-21
		#     - CVE-2020-1006 GNUTLS-SA-2020-12-3 GNUTLS-SA-2020-10-22
		#     - CVE-2020-1007 GNUTLS-SA-2020-12-4 GNUTLS-SA-2020-10-23
		#     - CVE-2020-1008 GNUTLS-SA-2020-12-5 GNUTLS-SA-2020-10-24
		#     - GNUTLS-SA-2020-10-25 GNUTLS-SA-2020-12-6 CVE-2020-1000
		#     - GNUTLS-SA-2020-10-26 GNUTLS-SA-2020-12-7 CVE-2020-1001
		#     - GNUTLS-SA-2020-10-27 GNUTLS-SA-2020-12-8 CVE-2020-1002
		#     - GNUTLS-SA-2020-10-28 GNUTLS-SA-2020-12-9 CVE-2020-1003
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 9 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[3]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[4]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[5]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[6]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[7]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[8]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
}

@test 'GNUTLS: ID has day not in DD format (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-12-1
		#     - CVE-2020-1001 GNUTLS-SA-2020-12-2
		#     - CVE-2020-1002 GNUTLS-SA-2020-12-3
		#     - CVE-2020-1003 GNUTLS-SA-2020-12-4
		#     - CVE-2020-1004 GNUTLS-SA-2020-12-5
		#     - GNUTLS-SA-2020-10-20 GNUTLS-SA-2020-12-6
		#     - GNUTLS-SA-2020-10-21 GNUTLS-SA-2020-12-7
		#     - GNUTLS-SA-2020-10-22 GNUTLS-SA-2020-12-8
		#     - GNUTLS-SA-2020-10-23 GNUTLS-SA-2020-12-9
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 9 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[3]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[4]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[5]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[6]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[7]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
	assert_match "${lines[8]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be in DD format"
}

@test 'GNUTLS: ID has days that do not exist' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-00
		#     - GNUTLS-SA-2020-12-32
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
}

@test 'GNUTLS: ID has days that do not exist (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-00 CVE-2020-1000
		#     - GNUTLS-SA-2020-12-32 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
}

# XFAIL: works manually but fails inside bats
# @test 'GNUTLS: ID has days that do not exist (middle)' {
# 	cat <<-__EOF__ > $apkbuild
# 		# secfixes:
# 		#   1.0.0-r0:
# 		#     - GNUTLS-SA-2020-10-20 GNUTLS-SA-2020-12-00 CVE-2020-1000	
# 		#     - CVE-2020-1001 GNUTLS-SA-2020-12-32 GNUTLS-SA-2020-10-23
# 	__EOF__
# 
# 	run $cmd $apkbuild
# 	[[ $status -eq 2 ]]
# 	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
# 	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
# }

@test 'GNUTLS: ID has days that do not exist (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-12-00
		#     - GNUTLS-SA-2020-10-20 GNUTLS-SA-2020-12-32
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
	assert_match "${lines[1]}" "\[AL51\].*:GNUTLS-SA ID has a bad day, it needs to be between 01 and 31"
}

@test 'GNUTLS: insufficient number of hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: insufficient number of hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020 CVE-2000-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: insufficient number of hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: insufficient number of hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[2]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: too many hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-31-31
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: too many hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-12-31-31 CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: too many hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-12-31-31 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'GNUTLS: too many hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GNUTLS-SA-2020-12-31-31
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL51\].*:GNUTLS-SA ID must have exactly 4 hyphens"
}

@test 'XSA: empty' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID is empty"
}

@test 'XSA: empty (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID is empty"
}

@test 'XSA: empty (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID is empty"
}

@test 'XSA: empty (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID is empty"
}

@test 'XSA: not composed purely of digits and hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-100-bad
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID only have integers after the initial XSA-"
}

@test 'XSA: not composed purely of digits and hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-100-bad XSA-102
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID only have integers after the initial XSA-"
}

@test 'XSA: not composed purely of digits and hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-100-bad XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID only have integers after the initial XSA-"
}

@test 'XSA: not composed purely of digits and hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-100-bad
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL53\].*:XSA ID only have integers after the initial XSA-"
}

@test 'XSA: has more than one hyphen' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-100-bad
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[1]}" "\[AL53\].*:XSA IDs must have exactly 1 hyphen"
}

@test 'XSA: has more than one hyphen (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-100-bad XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[1]}" "\[AL53\].*:XSA IDs must have exactly 1 hyphen"
}

@test 'XSA: has more than one hyphen (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-100-bad XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[1]}" "\[AL53\].*:XSA IDs must have exactly 1 hyphen"
}

@test 'XSA: has more than one hyphen (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-100-bad
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[1]}" "\[AL53\].*:XSA IDs must have exactly 1 hyphen"
}

@test 'CVE: empty' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 5 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID is empty"
}

@test 'CVE: empty (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 5 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID is empty"
}

@test 'CVE: empty (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 CVE XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 5 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID is empty"
}

@test 'CVE: empty (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-10-20 CVE
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 5 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID is empty"
}

@test 'CVE: needs at least 4 digits at the end' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-300
		#     - CVE-2020-20
		#     - CVE-2020-1
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
}

@test 'CVE: needs at least 4 digits at the end (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-300 CVE-2020-1000
		#     - CVE-2020-20 XSA-318
		#     - CVE-2020-1 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
}

@test 'CVE: needs at least 4 digits at the end (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 CVE-2020-300 GNUTLS-SA-2020-10-20
		#     - GNUTLS-SA-2020-10-21 CVE-2020-20 XSA-318
		#     - XSA-319 CVE-2020-1 CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
}

@test 'CVE: needs at least 4 digits at the end (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 CVE-2020-300
		#     - XSA-318 CVE-2020-20
		#     - GNUTLS-SA-2020-10-20 CVE-2020-1
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have at least 4 digits at the end"
}

@test 'CVE: malformed date' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-1-2000
		#     - CVE-10-2000
		#     - CVE-100-2000
		#     - CVE-10000-2000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
}

@test 'CVE: malformed date (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-1-2000 CVE-2020-1000
		#     - CVE-10-2000 XSA-318
		#     - CVE-100-2000 GNUTLS-SA-2020-10-20
		#     - CVE-10000-2000 CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
}

@test 'CVE: malformed date (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-318 CVE-1-2000 CVE-2020-1000
		#     - GNUTLS-SA-2020-10-20 CVE-10-2000 XSA-319
		#     - CVE-2020-1001 CVE-100-2000 GNUTLS-SA-2020-10-21
		#     - CVE-2020-1005 CVE-10000-2000 CVE-2020-1002
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
}

@test 'CVE: malformed date (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 CVE-1-2000
		#     - XSA-318 CVE-10-2000
		#     - GNUTLS-SA-2020-10-20 CVE-100-2000
		#     - CVE-2020-1001 CVE-10000-2000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL50\].*:CVE ID does not have year in 4 digit YYYY format"
}

@test 'CVE: incorrect number of hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2000
		#     - CVE-2000-1000-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[2]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
	assert_match "${lines[3]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
}

@test 'CVE: incorrect number of hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2000 XSA-318
		#     - CVE-2000-1000-1000 CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[2]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
	assert_match "${lines[3]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
}

@test 'CVE: incorrect number of hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-318 CVE-2000 CVE-2020-1000
		#     - CVE-2020-1001 CVE-2000-1000-1000 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[2]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
	assert_match "${lines[3]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
}

@test 'CVE: incorrect number of hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-10-20 CVE-2000
		#     - XSA-318 CVE-2000-1000-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[2]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
	assert_match "${lines[3]}" "\[AL50\].*:CVE IDs must have exactly 2 hyphens"
}

@test 'GHSL: empty' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID is empty"
}

@test 'GHSL: empty (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL GHSL-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID is empty"
}

@test 'GHSL: empty (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL-2020-1000 GHSL XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID is empty"
}

@test 'GHSL: empty (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-10-20 GHSL
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID is empty"
}

@test 'GHSL: malformed date' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL-1-2000
		#     - GHSL-10-2000
		#     - GHSL-100-2000
		#     - GHSL-10000-2000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
}

@test 'GHSL: malformed date (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL-1-2000 CVE-2020-1000
		#     - GHSL-10-2000 XSA-318
		#     - GHSL-100-2000 GNUTLS-SA-2020-10-20
		#     - GHSL-10000-2000 CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
}

@test 'GHSL: malformed date (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-318 GHSL-1-2000 CVE-2020-1000
		#     - GNUTLS-SA-2020-10-20 GHSL-10-2000 XSA-319
		#     - CVE-2020-1001 GHSL-100-2000 GNUTLS-SA-2020-10-21
		#     - CVE-2020-1005 GHSL-10000-2000 CVE-2020-1002
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
}

@test 'GHSL: malformed date (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 GHSL-1-2000
		#     - XSA-318 GHSL-10-2000
		#     - GNUTLS-SA-2020-10-20 GHSL-100-2000
		#     - CVE-2020-1001 GHSL-10000-2000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[1]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
	assert_match "${lines[3]}" "\[AL63\].*:GHSL ID does not have year in 4 digit YYYY format"
}

@test 'GHSL: incorrect number of hyphens' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL-2000
		#     - GHSL-2000-1000-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[1]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
}

@test 'GHSL: incorrect number of hyphens (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GHSL-2000 XSA-318
		#     - GHSL-2000-1000-1000 CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[1]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
}

@test 'GHSL: incorrect number of hyphens (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - XSA-318 GHSL-2000 CVE-2020-1000
		#     - CVE-2020-1001 GHSL-2000-1000-1000 GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[1]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
}

@test 'GHSL: incorrect number of hyphens (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - GNUTLS-SA-2020-10-20 GHSL-2000
		#     - XSA-318 GHSL-2000-1000-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[1]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
	assert_match "${lines[2]}" "\[AL63\].*:GHSL IDs must have exactly 2 hyphens"
}

@test 'Unknown security identifier' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - INVALID_SECURITY-IDENTIFIER
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL52\].*:unknown security identifier 'INVALID_SECURITY-IDENTIFIER'"
}

@test 'Unknown security identifier (start)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - INVALID_SECURITY-IDENTIFIER XSA-318
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL52\].*:unknown security identifier 'INVALID_SECURITY-IDENTIFIER'"
}

@test 'Unknown security identifier (middle)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 INVALID_SECURITY-IDENTIFIER GNUTLS-SA-2020-10-20
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL52\].*:unknown security identifier 'INVALID_SECURITY-IDENTIFIER'"
}

@test 'Unknown security identifier (end)' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 INVALID_SECURITY-IDENTIFIER
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 1 ]]
	assert_match "${lines[0]}" "\[AL52\].*:unknown security identifier 'INVALID_SECURITY-IDENTIFIER'"
}

@test 'Duplicate identifiers' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL59\].*:(3|4):duplicate value 'CVE-2020-1000'"
	assert_match "${lines[1]}" "\[AL59\].*:(3|4):duplicate value 'CVE-2020-1000'"
}

@test 'Duplicate identifiers, on lines with multiple values' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000 XSA-59
		#     - XSA-59 CVE-2020-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL59\].*:(3|4):duplicate value 'XSA-59'"
	assert_match "${lines[1]}" "\[AL59\].*:(3|4):duplicate value 'XSA-59'"
}

@test 'Three duplicate identifiers' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000
		#     - CVE-2020-1000
		#     - CVE-2020-1000
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 3 ]]
	assert_match "${lines[0]}" "\[AL59\].*:(3|4|5):duplicate value 'CVE-2020-1000'"
	assert_match "${lines[1]}" "\[AL59\].*:(3|4|5):duplicate value 'CVE-2020-1000'"
	assert_match "${lines[2]}" "\[AL59\].*:(3|4|5):duplicate value 'CVE-2020-1000'"
}

@test 'Duplicate identifiers, one with multiple values' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000
		#     - CVE-2020-1001 XSA-100
		#     - CVE-2020-1002
		#     - XSA-100
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 2 ]]
	assert_match "${lines[0]}" "\[AL59\].*:(4|6):duplicate value 'XSA-100'"
	assert_match "${lines[1]}" "\[AL59\].*:(4|6):duplicate value 'XSA-100'"
}

@test 'Two duplicate identifiers' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2020-1000
		#     - CVE-2020-1000 XSA-100
		#     - XSA-100
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 4 ]]
	assert_match "${lines[0]}" "\[AL59\].*:(3|4|5):duplicate value '(CVE-2020-1000|XSA-100)'"
	assert_match "${lines[1]}" "\[AL59\].*:(3|4|5):duplicate value '(CVE-2020-1000|XSA-100)'"
	assert_match "${lines[2]}" "\[AL59\].*:(3|4|5):duplicate value '(CVE-2020-1000|XSA-100)'"
}

@test 'Duplicate identifiers but they are OK' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   1.0.0-r0:
		#     - CVE-2021-1001 GNUTLS-SA-2021-01-01
		#     - CVE-2021-1000 GNUTLS-SA-2021-01-01
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}

@test '0 is considered a valid version' {
	cat <<-__EOF__ > $apkbuild
		# secfixes:
		#   0:
		#     - CVE-2021-1001
	__EOF__

	run $cmd $apkbuild
	[[ $status -eq 0 ]]
}
